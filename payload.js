var restify = require('restify'),
    simpleGit = require('simple-git');

var remote = "git@repo",
    branch = "origin/master";


function payload(req, res, next) {
  //console.log(req, res, next);
  require('simple-git')(__dirname + '/mainsite')
     .pull()
     .tags(function(err, tags) {
        console.log("Latest available tag: %s", tags.latest);
     });
}

var server = restify.createServer();

server.get('/payload', payload);
server.post('/payload', payload);

server.on('uncaughtException', function (req, res, route, err) {
    console.log('uncaughtException', err.stack);
});

server.listen(8080, function() {
    console.log('%s listening at %s', server.name, server.url);
});
