Simple Node Webhook

Refer to this [blog post](http://www.bdodev.com/2016/03/bitbucket-webhooks-and-node-yep.html) for setting up git and webhooks front end.

# SETUP

install:
- Node
- restify
- simple-git

Set payload.js remote and branch variables,
then run it from command line: node payload.js

## runOnReboot.sh

A quick little sh script I run on reboot. For some reason my ssh setup requires I create a new agent on reboot and attach the key to it.
Annoying but this was the quickest way to "fix" it. And really, how often does your server go down anyway right?.....right?

To use: configure the key name on line 5 and run ./runOnReboot.sh on reboot. It will ask you to input the ssh password then it will run payload.js
